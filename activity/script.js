// S24 Activity Template:
	/*Item 1.)
		- Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
		- Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
		*/

	// Code here:

const getCube = function (number) {
	const cube = number ** 3;
	return console.log(`The cube of ${number} is ${cube}.`)

}

getCube(2);



	/*Item 2.)
		- Create a variable address with a value of an array containing details of an address.
		- Destructure the array and print out a message with the full address using Template Literals.*/


	// Code here:

	let address = ["B9 L21", "Amazing Street", "Excellent Village", "Awesome City", "Philippines"]

	address = [block,street,village,city,country] = address;

	 console.log(`Here is my address ${block}, ${street}, ${village}, ${city}, ${country}.`)


	/*Item 3.)
		- Create a variable animal with a value of an object data type with different animal details as its properties.
		- Destructure the object and print out a message with the details of the animal using Template Literals.
*/
	// Code here:

let animal = {
	name: "Cat",
	food: "fish",
	personality: ["playful", "independent"]
}

animal = {name, food, personality} = animal;

console.log(`${name}s are ${personality[0]} and ${personality[1]}. They love ${food}.`)



	/*Item 4.)
		- Create an array of numbers.
		- Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.*/

	// Code here:

const numbers = [2, 4, 6, 8, 10];

numbers.forEach(number => console.log(number));


/*
	Item 5.)
		- Create a class of a Dog and a constructor that will accept a name, age and breed as its properties.
		- Create/instantiate a new object from the class Dog and console log the object.*/

	// Code here:

class Dog {
	constructor(name, age, breed) {
		this.dogName = name,
		this.dogAge = age,
		this.dogBreed = breed
	}
}

const listDog = new Dog("Miggy", 7, "Aspin");
console.log(listDog);






